﻿using System;

namespace Tam.Lib.Extensions
{
	public static class StringExtensions
	{
		/// <summary>
		/// Checks if a string contains a value using the given string comparison type
		/// </summary>
		/// <param name="source">The string to search</param>
		/// <param name="value">The value to search for</param>
		/// <param name="comparisonType">The comparison type</param>
		/// <returns>Whether the string contains the given value</returns>
		public static bool Contains(this string source, string value, StringComparison comparisonType)
		{
			return (source.IndexOf(value, comparisonType) >= 0);
		}

		/// <summary>
		/// Checks if a url is relative to the root
		/// </summary>
		/// <param name="source">The url to check</param>
		/// <returns>Whether the url is relative to the root</returns>
		public static bool IsRelativeToRoot(this string source)
		{
			return (source.Length > 1 && source.StartsWith("/")
				&& !source.StartsWith("//") && !source.StartsWith("/\\"));
		}
	}
}
